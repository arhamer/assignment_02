# README #

% These are functions that cslculate via binomial method the following for 
% European and American style options:
%
% Call price
% Put price
% Call delta
% Put delta
% Call Gamma
% Put Gamma
%
% K = Strike price per share in home currency
% T = Tenor - time to expiration input as decimal years
% S = Underlying price per share in home currency
% sig = Volatility - standard deviation of the underlying input as decimal
% r = Riskfree interest rate continuously compounded input as decimal 
% N = Number of steps to calculate. In the interest of time this is NOT TO EXCEED 1000