function [call_price,put_price,call_delta,put_delta,call_gamma,put_gamma]=Euro_Additive(K,T,S,sig,r,N)
% This is a function that calculates via binomial method the following for 
% a European style option:
%
% Call price
% Put price
% Call delta 
% Put delta
% Call Gamma
% Put Gamma
%
% K = Strike price per share in home currency
% T = Tenor - time to expiration input as decimal years
% S = Underlying price per share in home currency
% sig = Volatility - standard deviation of the underlying input as decimal
% r = Riskfree interest rate continuously compounded input as decimal 
% N = Number of steps to calculate. In the interest of time this is NOT TO EXCEED 1000

if N>1001
    error('In the interest of time Please input 1000 or less steps for this simulation.')
end 

%set time coefficents
dt=T/N;
nu=r-(0.5*sig^2);
dtu=sqrt(sig^2*dt+(nu*dt)^2);
dtd=-dtu;
pu=1/2+1/2*(nu*dt/dtu);
pd=1-pu;

% precompute constants
disc=exp(-r.*dt);
edxud=exp(dtu-dtd);
edxd=exp(dtd);

% initialise asset prices at maturity time step N
St=zeros(N+1,1);
St(1)=S*exp(N*dtd);
for j=2:N+1
    St(j)=St(j-1)*edxud;
end

% initialise option values at maturity
C=zeros(N+1,1);
P=zeros(N+1,1);
for j=1:N+1
    P(j)=max(0.0,K-St(j));
    C(j)=max(0.0,St(j)-K);
end

%step back through the T tree
for i=N:-1:1
   for j=1:i
      C(j)=disc*(pu*C(j+1)+pd*C(j));
      P(j)=disc*(pu*P(j+1)+pd*P(j));
      
      % adjust asset price current time stamp
      St(j)=St(j)/edxd;
   end    
end

%retrieve discounted call and put prices
call_price=C(1)
put_price=P(1)

% calculate hedge sensitivities
call_delta=(C(2)-C(1))/(St(2)-St(1))
put_delta=(P(2)-P(1))/(St(2)-St(1))
call_gamma=((C(3)-C(2))/(St(3)-St(2))-(C(2)-C(1))/(St(2)-St(1)))/(1/2*(St(3)-St(1)))
put_gamma=((P(3)-P(2))/(St(3)-St(2))-(P(2)-P(1))/(St(2)-St(1)))/(1/2*(St(3)-St(1)))

end
